package com.example.alex.destinyquoz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ResultsActivity extends AppCompatActivity {

    TextView txtViewOne;
    TextView txtViewTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        txtViewOne = (TextView) findViewById(R.id.firstTxtView);
        txtViewTwo = (TextView) findViewById(R.id.secondTxtView);
        Intent i = getIntent();
    }
    public void save(View v)
    {
        String value = getIntent().getStringExtra("q1Value");
        String valuetwo = getIntent().getStringExtra("q2Value");
        String valuethree = getIntent().getStringExtra("q3Value");
        String valuefour = getIntent().getStringExtra("q4Value");
        DBHelperClass db = new DBHelperClass(this);
        db.AddValues(new DBContract(value,valuetwo,valuethree,valuefour));
        Toast.makeText(getBaseContext(),"Values added",Toast.LENGTH_SHORT).show();

    }
}
