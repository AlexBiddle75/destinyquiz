package com.example.alex.destinyquoz;

import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class secondQuestion extends AppCompatActivity {


    private RadioGroup radioGroup;
    private RadioButton rbTop;
    private RadioButton rbCenter;
    private RadioButton rbBottom;
    private Button btnNext;
    private String userValue;
    private GLSurfaceView glSurfaceView;
    String firstQuestionData;

    TextView txtvView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_question);
        txtvView = (TextView) findViewById(R.id.txtView);
        btnNext = (Button) findViewById(R.id.btnSecondQuestion);
        rbTop = (RadioButton) findViewById(R.id.rbTop);
        rbCenter = (RadioButton) findViewById(R.id.rbCenter);
        rbBottom = (RadioButton) findViewById(R.id.rbBottom);
        radioGroup = (RadioGroup) findViewById(R.id.rgHeader);
        Intent getFirstQuestion = getIntent();
        firstQuestionData = getFirstQuestion.getStringExtra("q1Value");

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.rbTop){
                    Toast.makeText(getBaseContext(),"Hunter clicked",Toast.LENGTH_SHORT).show();
                    userValue = "40";
                }
                else if (i==R.id.rbCenter)
                {
                    Toast.makeText(getBaseContext(),"Titan clicked",Toast.LENGTH_SHORT).show();
                    userValue = "50";
                }
                else if (i==R.id.rbBottom)
                {
                    Toast.makeText(getBaseContext(),"Warlock clicked",Toast.LENGTH_SHORT).show();
                    userValue = "60";
                }
            }
        });
    }

    public void onClick(View v)
    {
        //integer which gets of radiogroup button which is clicked
        int id= radioGroup.getCheckedRadioButtonId();
        Intent goToQuestionTwo = new Intent(secondQuestion.this,thirdQuestion.class);
        //contains value contains value to go into value and will be measured against criteria
        //for scores
        goToQuestionTwo.putExtra("q2Value",userValue);
        goToQuestionTwo.putExtra("q1Value",firstQuestionData);
        startActivity(goToQuestionTwo);

    }
}
