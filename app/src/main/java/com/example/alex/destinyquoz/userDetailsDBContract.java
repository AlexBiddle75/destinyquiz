package com.example.alex.destinyquoz;

/**
 * Created by Alex on 23/12/2016.
 */

public class userDetailsDBContract {

    public static String userEmail;
    public static String userName;
    public static String userPassword;

    public userDetailsDBContract(String email,String password)
    {
        this.userEmail = email;
        this.userPassword = password;

    }
    public static String getUserEmail()
    {
        return userEmail;
    }

    public static String getUserPassword()
    {
        return userPassword;
    }

    public static String getUserName(){
        return userName;
    }


}
