package com.example.alex.destinyquoz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

public class registerActivity extends AppCompatActivity {

    private Button btnRegister;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtUserName;
    private String getUserEmail;
    private String getUserPassword;
    private String getUserName;
    private String userEmail;
    private String userPassword;
    private ProgressDialog progressRegister;
    private FirebaseAuth fireBaseAuth;
    private String userName = "Username";
    private String userDetailsTableName = "userDetails";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressRegister = new ProgressDialog(this);
        fireBaseAuth = FirebaseAuth.getInstance();
        btnRegister = (Button) findViewById(R.id.btnRegister);
        txtEmail = (EditText) findViewById(R.id.editTxtName);
        txtPassword = (EditText) findViewById(R.id.editTxtPassword);
        txtUserName = (EditText) findViewById(R.id.editUserName);
    }

    private void register()
    {
        userEmail = txtEmail.getText().toString().trim();
        userPassword = txtPassword.getText().toString().trim();
        userName = txtUserName.getText().toString().trim();
        progressRegister.setMessage("Register user:");
        progressRegister.show();
    }

    /**
     * Method will contain code that is used to register a user
     * @param v
     */

    public void registerClick(View v)
    {
        //creating instance of helper class
        DBHelperClass db = new DBHelperClass(this);
        //setting string variable equal to value entered in edit text
        getUserEmail = txtEmail.getText().toString();
        getUserName = txtUserName.getText().toString();
        getUserPassword = txtPassword.getText().toString();
        Toast.makeText(this, "Button clicked", Toast.LENGTH_SHORT).show();
        //validation
        if (TextUtils.isEmpty(getUserEmail))
        {
            Toast.makeText(this, "Please enter an email address", Toast.LENGTH_SHORT).show();
        }

        if (TextUtils.isEmpty(getUserName))
        {
            Toast.makeText(this, "Please enter a username", Toast.LENGTH_SHORT).show();
        }

        if (TextUtils.isEmpty(getUserPassword))
        {
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_SHORT).show();
        }

        if (getUserPassword.length()<8)
        {
            Toast.makeText(getBaseContext(),"Please enter a password of 8 characters or more",Toast.LENGTH_SHORT);
        }
        //runs method of helper class instance to register user
        db.AddUserValues(new userDetailsDBContract(getUserEmail,getUserPassword));
        progressRegister.dismiss();
        //boolean to check if username has been taken
        boolean isTrue =  db.isDataInDB(userDetailsTableName,userName,getUserName);
        if (isTrue)
        {
            Toast.makeText(this, "Sorry, a user has already taken that username", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Intent startLogin = new Intent(this,loginActivity.class);
            startActivity(startLogin);
        }
        

    }

}
