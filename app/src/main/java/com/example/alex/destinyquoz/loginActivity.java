package com.example.alex.destinyquoz;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

public class loginActivity extends AppCompatActivity {

    private EditText editTxtEmail;
    private EditText editTxtPassword;
    private FirebaseAuth fireBaseAuth;
    private Button btnLogin;
    private String userEmail;
    private String userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fireBaseAuth = FirebaseAuth.getInstance();

        editTxtEmail = (EditText) findViewById(R.id.editTextEmail);
        editTxtPassword = (EditText) findViewById(R.id.editTextLoginPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });
    }
    public void loginUser()
    {
        DBHelperClass dbHelper = new DBHelperClass(this);
        userEmail = editTxtEmail.getText().toString();
        userPassword = editTxtPassword.getText().toString();
        if (TextUtils.isEmpty(userEmail))
        {
            Toast.makeText(getBaseContext(), "Please enter an email address", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(userPassword))
        {
            Toast.makeText(getBaseContext(), "Please enter a password", Toast.LENGTH_SHORT).show();
        }

        dbHelper.checkUserDetails(new userDetailsDBContract(userEmail,userPassword));
        Toast.makeText(this, "user is registered", Toast.LENGTH_SHORT).show();
        Intent goToFirstQuestion = new Intent(loginActivity.this,FirstQuestionActivity.class);
        goToFirstQuestion.putExtra("userEmail",userEmail);
        startActivity(goToFirstQuestion);

    }


    public void goToRegister(View v)
    {
        Intent goToRegister = new Intent(loginActivity.this,registerActivity.class);
        startActivity(goToRegister);
    }
}
