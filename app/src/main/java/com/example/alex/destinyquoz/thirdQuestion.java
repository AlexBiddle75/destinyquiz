package com.example.alex.destinyquoz;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class thirdQuestion extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton rbTop;
    private RadioButton rbCenter;
    private RadioButton rbBottom;
    private Button btnToResults;
    private TextView txtView;
    private String userValue;
    String firstQuestionData;
    String secondQuestionData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_question);
        btnToResults = (Button) findViewById(R.id.btnThirdQuestion);
        txtView = (TextView) findViewById(R.id.testTxtView);
        rbTop = (RadioButton) findViewById(R.id.rbTop);
        rbCenter = (RadioButton)findViewById(R.id.rbCenter);
        rbBottom = (RadioButton) findViewById(R.id.rbBottom);
        radioGroup = (RadioGroup) findViewById(R.id.rgHeader);
        Intent getQuestionData = getIntent();
        final String QuestionOneData = getQuestionData.getStringExtra("q1Value");
        final String QuestionTwoData = getQuestionData.getStringExtra("q2Value");


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (i==R.id.rbTop){

                    userValue = "Suros Regime";
                }
                else if (i==R.id.rbCenter)
                {

                    userValue = "Bad juju";
                }
                else if (i==R.id.rbBottom)
                {

                    userValue = "Devils breath";
                }
            }
        });

        btnToResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(thirdQuestion.this,fourth_Question.class);
                i.putExtra("q3Value",userValue);
                i.putExtra("q2Value",QuestionTwoData);
                i.putExtra("q1Value",QuestionOneData);
                startActivity(i);
            }
        });

    }






}


