package com.example.alex.destinyquoz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class FirstQuestionActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton rbTop;
    private RadioButton rbCenter;
    private RadioButton rbBottom;
    private Button btnNext;
    private String containsValue;
    TextView txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent getEmail = getIntent();
        String users = getEmail.getExtras().getString("userEmail");
        txtView = (TextView) findViewById(R.id.txtViewTest);

        btnNext = (Button) findViewById(R.id.btnSecondQuestion);
        setContentView(R.layout.activity_first_question);
        radioGroup = (RadioGroup) findViewById(R.id.rgHeader);
        rbTop = (RadioButton) findViewById(R.id.rbTop);
        rbCenter = (RadioButton) findViewById(R.id.rbCenter);
        rbBottom = (RadioButton) findViewById(R.id.rbBottom);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.rbTop){
                    Toast.makeText(getBaseContext(),"Hunter clicked",Toast.LENGTH_SHORT).show();
                    containsValue = "Hunter";
                }
                else if (i==R.id.rbCenter)
                {
                    Toast.makeText(getBaseContext(),"Titan clicked",Toast.LENGTH_SHORT).show();
                    containsValue = "Titan";
                }
                else if (i==R.id.rbBottom)
                {
                    Toast.makeText(getBaseContext(),"Warlock clicked",Toast.LENGTH_SHORT).show();
                    containsValue = "Warlock";
                }
            }
        });
    }

    public void onClick(View v)
    {
        //integer which gets of radiogroup button which is clicked
        int id= radioGroup.getCheckedRadioButtonId();
        Intent goToQuestionTwo = new Intent(FirstQuestionActivity.this,secondQuestion.class);
        //contains value contains value to go into value and will be measured against criteria
        //for scores
        goToQuestionTwo.putExtra("q1Value",containsValue);
        startActivity(goToQuestionTwo);

    }



}
