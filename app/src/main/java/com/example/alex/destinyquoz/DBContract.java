package com.example.alex.destinyquoz;

/**
 * Created by Alex on 17/12/2016.
 */

public class DBContract {

    public static String tableName;
    public static String q1Answer;
    public static String q2Answer;
    public static String q3Answer;
    public static String q4Answer;
    public static String q5Answer;
    public static String q6Answer;
    public static String q7Answer;
    public static String q8Answer;
    public static String q9Answer;
    public static String q10Answer;


    public DBContract(String q1Answer, String q2Answer, String q3Answer,String q4Answer){
        this.q1Answer = q1Answer;
        this.q2Answer = q2Answer;
        this.q3Answer = q3Answer;
        this.q4Answer = q4Answer;
    }

    public static String getQ1Answer()
    {
        return q1Answer;
    }
    public static String getQ2Answer()
    {
        return q2Answer;
    }
    public static String getQ3Answer()
    {
        return q3Answer;
    }
    public static String getQ4Answer()
    {
        return q4Answer;
    }
//
//
//    public static String getQ5Answer()
//    {
//        return q5Answer;
//    }
//    public static String getQ6Answer()
//    {
//        return q6Answer;
//    }
//
//
//    public static String getQ7Answer()
//    {
//        return q7Answer;
//    }
//    public static String getQ8Answer()
//    {
//        return q8Answer;
//    }
//
//    public static String getQ9Answer()
//    {
//        return q9Answer;
//    }
//
//    public static String getQ10Answer()
//    {
//        return q10Answer;
//    }





}
