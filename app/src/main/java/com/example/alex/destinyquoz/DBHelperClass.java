package com.example.alex.destinyquoz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Alex on 17/12/2016.
 */
public class DBHelperClass extends SQLiteOpenHelper {

    /**
     * The constant dbName.
     */
//setting database variables including database name, table name
    public static String dbName = "results";
    /**
     * The constant tableName.
     */
    public static final String tableName = "quizResults";
    /**
     * The constant userDetailsTableName.
     */
    public static final String userDetailsTableName = "userDetails";
    /**
     * The constant userID.
     */
    public static final String userID = "userID";
    /**
     * The constant userEmail.
     */
    public static final String userEmail = "Email";
    /**
     * The constant userName.
     */
    public static final String userName = "Username";
    /**
     * The constant userPassword.
     */
    public static final String userPassword = "Password";
    /**
     * The constant quizID.
     */
    public static final String quizID = "quizID";
    /**
     * The constant q1Answer.
     */
    public static final String q1Answer = "q1Answer";
    /**
     * The constant q2Answer.
     */
    public static final String q2Answer = "q2Answer";
    /**
     * The constant q3Answer.
     */
    public static final String q3Answer = "q3Answer";
    /**
     * The constant q4Answer.
     */
    public static final String q4Answer = "q4Answer";
    /**
     * The constant q5Answer.
     */
    public static final String q5Answer = "q5Answer";
    /**
     * The constant q6Answer.
     */
    public static final String q6Answer = "q6Answer";
    /**
     * The constant q7Answer.
     */
    public static final String q7Answer = "q7Answer";
    /**
     * The constant q8Answer.
     */
    public static final String q8Answer = "q8Answer";
    /**
     * The constant q9Answer.
     */
    public static final String q9Answer = "q9Answer";
    /**
     * The constant q10Answer.
     */
    public static final String q10Answer = "q10Answer";
    /**
     * The Db helper.
     */
    DBHelperClass dbHelper;
    /**
     * The Sql lite database.
     */
    SQLiteDatabase sqlLiteDatabase;

    /**
     * Instantiates a new Db helper class.
     *
     * @param context the context
     */
    public DBHelperClass(Context context) {
        super(context, dbName, null, 1);
    }

    /**
     * Overriden method which creates tables in database
     *
     * @param sqLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + tableName + "(" +
                quizID + " INTEGER PRIMARY KEY , " +
                q1Answer + " TEXT , " +
                q2Answer + " TEXT , " +
                q3Answer + " TEXT);"
        );

        sqLiteDatabase.execSQL("CREATE TABLE " + userDetailsTableName + "(" +
                userID + " INTEGER PRIMARY KEY , " +
                userName + "TEXT , " +
                userEmail + " TEXT , " +
                userPassword + " TEXT);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + tableName);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + userDetailsTableName);
    }

//    @Override
//    public void onDowngrade(SQLiteDatabase sqLiteDatabase,int i, int i1)
//    {
//
//    }

    /**
     * AddValues method which adds the values to the database#
     *
     * @param dbContract the db contract
     */
    public void AddValues(DBContract dbContract) {
        //creates database
        SQLiteDatabase db = getWritableDatabase();
        //create values set
        ContentValues dbValues = new ContentValues();
        //tells what values to insert
        dbValues.put(q1Answer, DBContract.getQ1Answer()); //first value is key, second is actual value
        dbValues.put(q2Answer, DBContract.getQ2Answer());
        dbValues.put(q3Answer, DBContract.getQ3Answer());
        dbValues.put(q4Answer, DBContract.getQ4Answer());
//        dbValues.put(q5Answer,DBContract.getQ5Answer());
//        dbValues.put(q6Answer,DBContract.getQ6Answer());
//        dbValues.put(q7Answer,DBContract.getQ7Answer());
//        dbValues.put(q8Answer,DBContract.getQ8Answer());
//        dbValues.put(q9Answer,DBContract.getQ9Answer());
//        dbValues.put(q10Answer,DBContract.getQ10Answer());
        //insert into table name, null if column is empty, then what values to actually insert
        db.insert(tableName, null, dbValues);
        //close database
        db.close();
    }

    /**
     * Add user values.
     *
     * @param userDetails the user details
     */
    public void AddUserValues(userDetailsDBContract userDetails) {
        //create database connection
        SQLiteDatabase userDB = getWritableDatabase();
        //create empty set of content values
        ContentValues userValues = new ContentValues();
        //insert user email
        userValues.put(userEmail, userDetailsDBContract.getUserEmail());
        userValues.put(userName, userDetailsDBContract.getUserName());
        //insert user password
        userValues.put(userPassword, userDetailsDBContract.getUserPassword());
        userDB.insert(userDetailsTableName, null, userValues);
        userDB.close();
    }

    /**
     * Check user details int.
     *
     * @param userDetails the user details
     * @return the int
     */
    public int checkUserDetails(userDetailsDBContract userDetails) {
        int dbValue = 0;
        //get database in read only mode
        SQLiteDatabase db = this.getReadableDatabase();
        //cursor allows read or write access to the output of a sql query
        Cursor cursor = db.rawQuery("SELECT userID FROM userDetails WHERE Email=? AND" +
                " Password=?", new String[]{userDetailsDBContract.getUserEmail(), userDetailsDBContract.getUserPassword()});

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            dbValue = cursor.getInt(0);
            cursor.close();
        }
        return dbValue;
    }

    /**
     * Is data in db boolean.
     *
     * @param tblName    the tbl name
     * @param dbField    the db field
     * @param fieldValue the field value
     * @return the boolean
     */
    public boolean isDataInDB(String tblName, String dbField, String fieldValue) {
        SQLiteDatabase db = this.getReadableDatabase();
        String isDataHereQuery = "SELECT * FROM " + tblName + "WHERE " + dbField + " = " + fieldValue;
        Cursor cursor = db.rawQuery(isDataHereQuery, null);
        //if there is more than instance of the result of the query i.e. if username is already in database
        if (cursor.getCount() <= 0) {
            //there is no username here
            cursor.close();
            return false;
        } else {
            //username here
            Log.d("usernameHere", "There is already a username here");
            return true;
        }
    }
}
