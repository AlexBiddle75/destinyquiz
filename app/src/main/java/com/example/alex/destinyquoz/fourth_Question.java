package com.example.alex.destinyquoz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class fourth_Question extends AppCompatActivity {

    private String userValue;
    private Button btnQuestionFour;
    private Spinner spinnerIronBanner;
    private String q1Value;
    private String q2Value;
    private String q3Value;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth__question);
        spinnerIronBanner = (Spinner) findViewById(R.id.spinnerIronBanner);
        Intent getPreviousValues = getIntent();
        q1Value = getPreviousValues.getStringExtra("q1Value");
        q2Value = getPreviousValues.getStringExtra("q2Value");
        q3Value = getPreviousValues.getStringExtra("q3Value");
        btnQuestionFour = (Button) findViewById(R.id.btnNext);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.ironBannerDays,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerIronBanner.setAdapter(arrayAdapter);
        spinnerIronBanner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i)
                {
                    case 0:
                        userValue = "one day";
                        break;
                    case 1:
                        userValue = "two days";
                        break;
                    case 2:
                        userValue = "three days";
                        break;
                    case 3:
                        userValue = "four days";
                        break;
                    case 4:
                        userValue = "five days";
                        break;
                    case 5:
                        userValue = "six days";
                        break;
                    case 6:
                        userValue = "seven days";
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void clickQuestionFour(View v)
    {
        Intent goToResults = new Intent(fourth_Question.this,ResultsActivity.class);
        goToResults.putExtra("q1Value",q1Value);
        goToResults.putExtra("q2Value",q2Value);
        goToResults.putExtra("q3Value",q3Value);
        goToResults.putExtra("q4Value",userValue);
        startActivity(goToResults);

    }
}
